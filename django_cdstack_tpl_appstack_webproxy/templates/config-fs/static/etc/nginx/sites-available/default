upstream app {
    server {{ app_server_ip }}:80;
}

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name _;
    
    root /var/www;

    location ^~ /.well-known/acme-challenge/ {
        access_log off;
        log_not_found off;

        proxy_pass {{ acme_forward_protocol }}acme;
        
        proxy_http_version 1.1;

        # Mitigating the HTTPoxy Vulnerability
        proxy_set_header Proxy "";

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 default_server ssl http2;
    listen [::]:443 default_server ssl http2;

    ssl_certificate /etc/ssl/certs/host.pem;
    ssl_certificate_key /etc/ssl/private/host.key;

    server_name _;
    
    root /var/www;

    location / {
{% for app_server_whitelist_ip in app_server_whitelist_ips %}        allow {{ app_server_whitelist_ip }};
{% endfor %}
{% for app_server_whitelist_ip6 in app_server_whitelist_ip6s %}        allow {{ app_server_whitelist_ip6 }};
{% endfor %}
        deny all;

        proxy_pass http://app;
        
        proxy_http_version 1.1;

        # Mitigating the HTTPoxy Vulnerability
        proxy_set_header Proxy "";

        proxy_set_header Host {{ app_server_header_host }};
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
